package concurrency

import (
	"log"
	"sync"
	"time"
)

type ForkMutex struct {
	ID int
	sync.Mutex
}

// PhilosopherMutex is a philosopher that uses a mutex to claim a form
type PhilosopherMutex struct {
	ID int
	L  *ForkMutex
	R  *ForkMutex
}

func (pm PhilosopherMutex) Run(n int) {
	for i := 0; i < n; i++ {
		log.Printf("Philosopher %d 🤤", pm.ID)

		pm.L.Lock()
		log.Printf("Philosopher %d 🤤🍴%d",
			pm.ID, pm.L.ID)

		pm.R.Lock()
		log.Printf("Philosopher %d 🍗🍴%d🍴%d",
			pm.ID, pm.L.ID, pm.R.ID)

		time.Sleep(time.Millisecond)

		pm.L.Unlock()
		pm.R.Unlock()

		log.Printf("Philosopher %d 🧠", pm.ID)
		time.Sleep(time.Millisecond)
	}
}
